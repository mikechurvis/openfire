Credits
	Developers
		Mike Churvis
Plugins Used
	Prototype
		ProCore
		[https://www.assetstore.unity3d.com/en/#!/content/11919]
	VFW
		Vexe
		[http://forum.unity3d.com/threads/open-source-vfw-134-drawers-save-system-serialize-interfaces-generics-autoprops-delegates.266165/]
	Rewired
		Guavaman Enterprises
		[https://www.assetstore.unity3d.com/en/#!/content/21676]
Special Thanks To:
	Sebastian Lague
		For his incredibly helpful tutorial series on procedural terrain asset generation in Unity.
	Mark Schoennagel
		For hosting the Summer 2015 Unity Roadshow in Atlanta, and giving me insight into how games become visually beautiful.
	