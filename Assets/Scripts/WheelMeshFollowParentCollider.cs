﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(WheelCollider))]
public class WheelMeshFollowParentCollider : MonoBehaviour
{
    public WheelCollider thisWheelCollider;
    public Transform childWheelMeshTransform;

    void Awake()
    {
        thisWheelCollider = GetComponent<WheelCollider>();
        FindChildWheelMesh();
        FollowCollider();
    }

    void Update()
    {
        FollowCollider();
    }

    public void FollowCollider()
    {
        if (childWheelMeshTransform)
        {
            Vector3 position;
            Quaternion rotation;

            thisWheelCollider.GetWorldPose(out position, out rotation);

            childWheelMeshTransform.position = position;
            childWheelMeshTransform.rotation = rotation;
        }
    }

    public void FindChildWheelMesh()
    {
        if (childWheelMeshTransform = transform.GetChild(0))
        {
            Debug.Log(transform.name + " found its wheel mesh.");
        }
        else
        {
            Debug.LogError(transform.name + " could not find a wheel mesh. Please make sure that the wheel mesh is "
                + "the first child in this collider's hierarchy.");
        }
    }
}
